import json
import requests
from ConfigAlpaca import *

BASE_URL = "https://paper-api.alpaca.markets"
ACCOUNT_URL = f"{BASE_URL}/v2/account"
ORDER_URL = f"{BASE_URL}/v2/orders"
CLOSEPOSITION = f"{BASE_URL}/v2/positions"
HEADERS = {'APCA-API-KEY-ID': API_KEY, 'APCA-API-SECRET-KEY': SECRET_KEY}


def getAccount():
    r = requests.get(ACCOUNT_URL, headers=HEADERS)
    return json.loads(r.content)


def getOrder(symbol, side, type, time_in_force, notional):
    data = {
        "symbol": symbol,
        "side": side,
        "type": type,
        "time_in_force": time_in_force,
        "notional": notional,

    }
    r = requests.post(ORDER_URL, json=data, headers=HEADERS)
    return json.loads(r.content)


def deletePosition(symbol, percentage, trail_percent):
    data = {
        "symbol": symbol,
        "percentage": percentage,
        "trail_percent": trail_percent,
    }
    r = requests.delete(CLOSEPOSITION, json=data, headers=HEADERS)
    return json.loads(r.content)
