from flask import Flask, request, abort
from flask_ngrok import run_with_ngrok

app = Flask(__name__)
run_with_ngrok(app)


@app.route('/webhook', methods=['POST'])
def webhook():
    if request.method == 'POST':
        print(request.json)
        return 'success', 200,
    else:
        abort(400)


if __name__ == '__main__':
    app.run()
