from Decoder import deco
from AlpacaOrder import getOrder, deletePosition
from datetime import datetime


def alpaca(SYMBOL, a):
    if a == "nada":
        order = {'order': 0}
        print(order)
    elif a == "buy":
        order = getOrder(symbol=f'{SYMBOL}', notional=10000, side=a, type="market", time_in_force="day")
        print(order, "buy", current_time)
        order = deletePosition(symbol=f'{SYMBOL}', percentage=100, trail_percent=1)
        print(order, "sell", current_time)


while True:
    SYMBOL, b, d = deco()
    print(SYMBOL, b, d)
    while True:
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        SYMBOL1, a, c = deco()
        print(SYMBOL1, a, c, current_time)
        if c != d:
            print(alpaca(SYMBOL, a), f"success {a}")
            break
